// Copyright 2018-2022 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <buildboxcommon_cashash.h>
#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_remoteexecutionclient.h>
#include <buildboxcommon_stringutils.h>

#include <google/rpc/code.pb.h>
#include <google/rpc/status.pb.h>

#include <fcntl.h>
#include <functional>
#include <unistd.h>
#include <unordered_map>
#include <vector>

#define POLL_WAIT std::chrono::seconds(1)

namespace buildboxcommon {

void RemoteExecutionClient::init(
    std::shared_ptr<Execution::StubInterface> executionStub,
    std::shared_ptr<ActionCache::StubInterface> actionCacheStub,
    std::shared_ptr<Operations::StubInterface> operationsStub)
{
    d_executionStub = executionStub;
    d_operationsStub = operationsStub;
    d_actionCacheStub = actionCacheStub;
}

void RemoteExecutionClient::init()
{
    d_executionStub =
        d_executionGrpcClient
            ? Execution::NewStub(d_executionGrpcClient->channel())
            : nullptr;
    d_operationsStub =
        d_executionGrpcClient
            ? Operations::NewStub(d_executionGrpcClient->channel())
            : nullptr;
    d_actionCacheStub =
        d_actionCacheGrpcClient
            ? ActionCache::NewStub(d_actionCacheGrpcClient->channel())
            : nullptr;
}

/**
 * Return the ActionResult for the given Operation. Throws an exception
 * if the Operation finished with an error, or if the Operation hasn't
 * finished yet.
 */
ActionResult getActionResult(const Operation &operation)
{
    if (!operation.done()) {
        throw std::logic_error(
            "Called getActionResult on an unfinished Operation");
    }
    else if (operation.has_error()) {
        throw GrpcError("Operation failed: " + operation.error().message(),
                        grpc::Status(static_cast<grpc::StatusCode>(
                                         operation.error().code()),
                                     operation.error().message()));
    }
    else if (!operation.response().Is<ExecuteResponse>()) {
        throw std::runtime_error("Server returned invalid Operation result");
    }

    ExecuteResponse executeResponse;
    if (!operation.response().UnpackTo(&executeResponse)) {
        throw std::runtime_error("Operation response unpacking failed");
    }

    const auto executeStatus = executeResponse.status();
    if (executeStatus.code() != google::rpc::Code::OK) {
        throw GrpcError(
            "Execution failed: " + executeStatus.message(),
            grpc::Status(static_cast<grpc::StatusCode>(executeStatus.code()),
                         executeStatus.message()));
    }

    const ActionResult actionResult = executeResponse.result();
    if (actionResult.exit_code() == 0) {
        BUILDBOX_LOG_DEBUG("Execute response message: " +
                           executeResponse.message());
    }
    else if (!executeResponse.message().empty()) {
        BUILDBOX_LOG_INFO("Remote execution message: " +
                          executeResponse.message());
    }

    return actionResult;
}

/**
 * Read the operation into the given pointer using async GRPC so we can
 * properly handle cancellation.
 *
 * cq->AsyncNext() blocks until the specified deadline is reached.
 * No built-in support for a stop signal. This means we need to
 * busy wait and check the signal flag on each iteration.
 */
bool RemoteExecutionClient::readOperation(
    grpc::CompletionQueue *cq, ReaderPointer &reader_ptr,
    OperationPointer &operation_ptr, const std::atomic_bool &stop_requested,
    const gpr_timespec &firstRequestDeadline, grpc::Status *errorStatus,
    bool wait)
{
    bool first = true;
    /**
     * Wait for the operation to complete, handling the
     * cancellation flag.
     */
    reader_ptr->Read(operation_ptr.get(), nullptr);
    while (wait || first) {
        void *tag;
        bool ok;
        const auto deadline = std::chrono::system_clock::now() + POLL_WAIT;
        const auto nextStatus = cq->AsyncNext(&tag, &ok, deadline);
        if (nextStatus == grpc::CompletionQueue::GOT_EVENT) {
            if (!ok) {
                /* No more messages in the stream */
                break;
            }
            if (first && !operation_ptr->name().empty()) {
                BUILDBOX_LOG_DEBUG(
                    "Waiting for Operation: " << operation_ptr->name())
            }
            first = false;
            if (operation_ptr->done()) {
                BUILDBOX_LOG_DEBUG("Operation done.");
                break;
            }
            /* Previous read is complete, start read of next message */
            reader_ptr->Read(operation_ptr.get(), nullptr);
        }
        else if (first && gpr_time_cmp(gpr_now(GPR_CLOCK_REALTIME),
                                       firstRequestDeadline) > 0) {
            /*
             * The first Operation response needs to arrive within the request
             * deadline, further updates may take a longer time.
             */
            cq->Shutdown();
            *errorStatus = grpc::Status(grpc::DEADLINE_EXCEEDED,
                                        "The Execute request timed out");
            return false;
        }
        else if (stop_requested) {
            cq->Shutdown();
            BUILDBOX_LOG_WARNING(
                "Cancelling job, operation name: " << operation_ptr->name());
            /* Cancel the operation if the execution service gave it a name */
            if (!operation_ptr->name().empty()) {
                cancelOperation(operation_ptr->name());
            }
            /* Return false to indicate failure */
            *errorStatus =
                grpc::Status(grpc::CANCELLED, "Operation was cancelled");
            return false;
        }
    }

    return true;
}

bool RemoteExecutionClient::fetchFromActionCache(
    const Digest &actionDigest, const std::set<std::string> &outputs,
    ActionResult *result)
{
    if (!d_actionCacheStub) {
        throw std::runtime_error("ActionCache Stub not Configured");
    }
    GetActionResultRequest actionRequest;
    actionRequest.set_instance_name(d_actionCacheGrpcClient->instanceName());

    actionRequest.set_inline_stdout(true);
    actionRequest.set_inline_stderr(true);
    for (const auto &o : outputs) {
        actionRequest.add_inline_output_files(o);
    }

    *actionRequest.mutable_action_digest() = actionDigest;

    ActionResult actionResult;
    bool found = false;

    auto getactionresult_lambda = [&](grpc::ClientContext &context) {
        const grpc::Status status = d_actionCacheStub->GetActionResult(
            &context, actionRequest, &actionResult);

        if (status.ok()) {
            found = true;
        }
        else if (status.error_code() == grpc::StatusCode::NOT_FOUND) {
            /* Return OK as this shouldn't throw an exception */
            return grpc::Status::OK;
        }

        return status;
    };

    d_actionCacheGrpcClient->issueRequest(
        getactionresult_lambda, "ActionCache.GetActionResult()", nullptr);

    if (found && result != nullptr) {
        *result = actionResult;
    }

    return found;
}

void RemoteExecutionClient::updateActionCache(const Digest &actionDigest,
                                              const ActionResult &result)
{
    if (!d_actionCacheStub) {
        throw std::runtime_error("ActionCache Stub not Configured");
    }
    UpdateActionResultRequest actionRequest;
    actionRequest.set_instance_name(d_actionCacheGrpcClient->instanceName());

    actionRequest.mutable_action_digest()->CopyFrom(actionDigest);
    actionRequest.mutable_action_result()->CopyFrom(result);

    auto updateactionresult_lambda = [&](grpc::ClientContext &context) {
        ActionResult actionResult;
        return d_actionCacheStub->UpdateActionResult(&context, actionRequest,
                                                     &actionResult);
    };

    d_actionCacheGrpcClient->issueRequest(updateactionresult_lambda,
                                          "ActionCache.UpdateActionResult()",
                                          nullptr);
}

OperationPointer RemoteExecutionClient::submitExecution(
    const Digest &actionDigest, const std::atomic_bool &stop_requested,
    bool skipCache, const ExecutionPolicy *executionPolicy, bool wait)
{
    if (!(d_executionStub && d_operationsStub)) {
        throw std::runtime_error("Execution Stubs not Configured");
    }

    /* Prepare an asynchronous Execute request */
    ExecuteRequest executeRequest;
    if (executionPolicy != nullptr) {
        executeRequest.mutable_execution_policy()->CopyFrom(*executionPolicy);
    }
    executeRequest.set_instance_name(d_executionGrpcClient->instanceName());
    *executeRequest.mutable_action_digest() = actionDigest;
    executeRequest.set_skip_cache_lookup(skipCache);

    OperationPointer operation_ptr;

    /* Create the lambda to pass to grpc_retry */
    auto execute_lambda = [&](grpc::ClientContext &context) {
        grpc::Status status;
        grpc::CompletionQueue cq;
        void *tag;
        bool ok;

        const auto requestDeadline = context.raw_deadline();

        /*
         * Don't use the configured request timeout as context deadline as that
         * would apply to the whole stream and would thus make it impossible
         * to execute long-running actions. Action timeouts can be set in the
         * `Action` message.
         * The request timeout will still be used for the first response, which
         * provides the operation name and should be returned by the server
         * without waiting for the execution of the action to complete.
         */
        context.set_deadline(gpr_inf_future(GPR_CLOCK_REALTIME));

        ReaderPointer reader_ptr = d_executionStub->AsyncExecute(
            &context, executeRequest, &cq, nullptr);
        const auto nextStatus = cq.AsyncNext(&tag, &ok, requestDeadline);
        if (nextStatus == grpc::CompletionQueue::TIMEOUT) {
            return grpc::Status(grpc::DEADLINE_EXCEEDED,
                                "Sending the Execute request timed out");
        }
        if (nextStatus != grpc::CompletionQueue::GOT_EVENT || !ok) {
            return grpc::Status(
                grpc::UNAVAILABLE,
                "Failed to send Execute request to the server");
        }

        /* Read the result of the Execute request into an OperationPointer */
        operation_ptr = std::make_shared<Operation>();
        if (!readOperation(&cq, reader_ptr, operation_ptr, stop_requested,
                           requestDeadline, &status, wait)) {
            return status;
        }

        if (wait) {
            reader_ptr->Finish(&status, nullptr);
            if (!cq.Next(&tag, &ok) || !ok) {
                return grpc::Status(grpc::UNAVAILABLE,
                                    "Failed to finish Execute client stream");
            }
        }

        return status;
    };

    d_executionGrpcClient->issueRequest(execute_lambda, "Execution.Execute()",
                                        nullptr);
    return operation_ptr;
}

ActionResult RemoteExecutionClient::executeAction(
    const Digest &actionDigest, const std::atomic_bool &stop_requested,
    bool skipCache, const ExecutionPolicy *executionPolicy)
{

    Operation operation = *submitExecution(actionDigest, stop_requested,
                                           skipCache, executionPolicy, true);
    if (!operation.done()) {
        throw std::runtime_error(
            "Server closed stream before Operation finished");
    }

    return getActionResult(operation);
}

google::longrunning::Operation RemoteExecutionClient::asyncExecuteAction(
    const Digest &actionDigest, const std::atomic_bool &stop_requested,
    bool skipCache, const ExecutionPolicy *executionPolicy)
{
    return *submitExecution(actionDigest, stop_requested, skipCache,
                            executionPolicy, false);
}

google::longrunning::Operation
RemoteExecutionClient::getOperation(const std::string &operationName)
{
    if (!(d_executionStub && d_operationsStub)) {
        throw std::runtime_error("Execution Stubs not Configured");
    }

    GetOperationRequest getRequest;
    getRequest.set_name(operationName);

    google::longrunning::Operation operation; // filled by lambda

    auto get_lambda = [&](grpc::ClientContext &context) {
        /* Send the get request and report any errors */
        return d_operationsStub->GetOperation(&context, getRequest,
                                              &operation);
    };

    d_executionGrpcClient->issueRequest(get_lambda,
                                        "Operations.GetOperation()", nullptr);

    return operation;
}

void RemoteExecutionClient::cancelOperation(const std::string &operationName)
{
    if (!(d_executionStub && d_operationsStub)) {
        throw std::runtime_error("Execution Stubs not Configured");
    }
    CancelOperationRequest cancelRequest;
    cancelRequest.set_name(operationName);

    auto cancel_lambda = [&](grpc::ClientContext &context) {
        /* Send the cancellation request and report any errors */
        google::protobuf::Empty empty;
        return d_operationsStub->CancelOperation(&context, cancelRequest,
                                                 &empty);
    };

    d_executionGrpcClient->issueRequest(
        cancel_lambda, "Operations.CancelOperation()", nullptr);

    BUILDBOX_LOG_INFO("Cancelled job " << operationName);
}

void checkDownloadBlobsResult(const CASClient::DownloadBlobsResult &results)
{
    std::vector<std::string> missingBlobs;

    for (const auto &result : results) {
        const auto &status = result.second.first;
        if (status.code() == grpc::StatusCode::NOT_FOUND) {
            missingBlobs.push_back(result.first);
        }
        else if (status.code() != grpc::StatusCode::OK) {
            throw GrpcError(
                "Failed to download output blob " + result.first + ": " +
                    std::to_string(status.code()) + ": " + status.message(),
                grpc::Status(static_cast<grpc::StatusCode>(status.code()),
                             status.message()));
        }
    }

    if (!missingBlobs.empty()) {
        std::ostringstream error;
        error << missingBlobs.size()
              << " output blobs missing from ActionResult: ";
        bool first = true;
        for (const auto hash : missingBlobs) {
            if (!first) {
                error << ", ";
            }
            error << hash;
            first = false;
        }
        throw GrpcError(error.str(), grpc::Status(grpc::StatusCode::NOT_FOUND,
                                                  error.str()));
    }
}

Digest addDirectoryToMap(std::unordered_map<Digest, Directory> *map,
                         const Directory &directory)
{
    const auto digest = CASHash::hash(directory.SerializeAsString());
    (*map)[digest] = directory;
    return digest;
}

void createParentDirectory(int dirfd, const std::string &path)
{
    const auto pos = path.rfind('/');
    if (pos != std::string::npos) {
        const std::string parent_path = path.substr(0, pos);
        FileUtils::createDirectory(dirfd, parent_path.c_str());
    }
}

void stageDownloadedFile(
    int dirfd, const std::string &path, const Digest &digest,
    bool is_executable, int temp_dirfd,
    const CASClient::DownloadBlobsResult &downloaded_files,
    const std::unordered_set<Digest> &duplicate_file_digests)
{
    auto temp_path = downloaded_files.at(digest.hash()).second;

    if (duplicate_file_digests.find(digest) != duplicate_file_digests.end()) {
        // Digest is used by multiple output files, create a copy
        auto temp_copy_path = temp_path + StringUtils::getRandomHexString();
        FileUtils::copyFile(temp_dirfd, temp_path.c_str(), temp_dirfd,
                            temp_copy_path.c_str());
        temp_path = temp_copy_path;
    }

    mode_t mode = 0644;
    if (is_executable) {
        mode |= S_IXUSR | S_IXGRP | S_IXOTH;
    }
    if (fchmodat(temp_dirfd, temp_path.c_str(), mode, 0) < 0) {
        BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
            std::system_error, errno, std::system_category,
            "Failed to set file mode of downloaded file");
    }
    if (renameat(temp_dirfd, temp_path.c_str(), dirfd, path.c_str()) < 0) {
        BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
            std::system_error, errno, std::system_category,
            "Failed to move downloaded file to final location: " << path);
    }
}

void stageDownloadedDirectory(
    int dirfd, const std::string &path, const Digest &dir_digest,
    int temp_dirfd,
    const std::unordered_map<Digest, Directory> &digest_directory_map,
    const CASClient::DownloadBlobsResult &downloaded_files,
    const std::unordered_set<Digest> &duplicate_file_digests)
{
    const auto &directory = digest_directory_map.at(dir_digest);
    FileUtils::createDirectory(dirfd, path.c_str());

    FileDescriptor current_dirfd(
        openat(dirfd, path.c_str(), O_RDONLY | O_DIRECTORY));
    if (current_dirfd.get() < 0) {
        BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
            std::system_error, errno, std::system_category,
            "Failed to open newly created subdirectory");
    }

    for (const auto &file_node : directory.files()) {
        stageDownloadedFile(current_dirfd.get(), file_node.name(),
                            file_node.digest(), file_node.is_executable(),
                            temp_dirfd, downloaded_files,
                            duplicate_file_digests);
    }
    for (const auto &dir_node : directory.directories()) {
        // Recursive call for subdirectory
        stageDownloadedDirectory(current_dirfd.get(), dir_node.name(),
                                 dir_node.digest(), temp_dirfd,
                                 digest_directory_map, downloaded_files,
                                 duplicate_file_digests);
    }
    for (const auto &symlink_node : directory.symlinks()) {
        if (symlinkat(symlink_node.target().c_str(), current_dirfd.get(),
                      symlink_node.name().c_str()) < 0) {
            BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(std::system_error, errno,
                                                  std::system_category,
                                                  "Failed to create symlink");
        }
    }
}

void RemoteExecutionClient::downloadOutputs(
    buildboxcommon::CASClient *casClient, const ActionResult &actionResult,
    int dirfd)
{
    std::unordered_set<Digest> tree_digests;
    for (const auto &dir : actionResult.output_directories()) {
        tree_digests.insert(dir.tree_digest());
    }

    const auto downloaded_trees = casClient->downloadBlobs(
        std::vector<Digest>(tree_digests.cbegin(), tree_digests.cend()));
    checkDownloadBlobsResult(downloaded_trees);

    std::unordered_set<Digest> file_digests, duplicate_file_digests;
    std::unordered_map<Digest, Directory> digest_directory_map;
    // Map from Digest of Tree to Digest of root directory of that tree
    std::unordered_map<Digest, Digest> tree_digest_root_digest_map;

    for (const auto &file : actionResult.output_files()) {
        const bool inserted = file_digests.insert(file.digest()).second;
        if (!inserted) {
            duplicate_file_digests.insert(file.digest());
        }
    }
    for (const auto &dir : actionResult.output_directories()) {
        Tree tree;
        const auto serialized_tree =
            downloaded_trees.at(dir.tree_digest().hash()).second;
        if (!tree.ParseFromString(serialized_tree)) {
            throw std::runtime_error("Could not deserialize downloaded Tree");
        }

        const auto root_digest =
            addDirectoryToMap(&digest_directory_map, tree.root());
        tree_digest_root_digest_map[dir.tree_digest()] = root_digest;

        for (const auto &tree_child : tree.children()) {
            addDirectoryToMap(&digest_directory_map, tree_child);
        }
    }

    for (const auto &digest_dir_iter : digest_directory_map) {
        // All directories are already in digest_directory_map, there is
        // no need for recursion.
        for (const auto &file_node : digest_dir_iter.second.files()) {
            const bool inserted =
                file_digests.insert(file_node.digest()).second;
            if (!inserted) {
                duplicate_file_digests.insert(file_node.digest());
            }
        }
    }

    const auto tempDirectoryName =
        ".reclient-" + StringUtils::getRandomHexString();

    if (mkdirat(dirfd, tempDirectoryName.c_str(), 0700) < 0) {
        BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
            std::system_error, errno, std::system_category,
            "Failed to create temporary directory");
    }
    try {
        FileDescriptor temp_dirfd(
            openat(dirfd, tempDirectoryName.c_str(), O_RDONLY | O_DIRECTORY));
        if (temp_dirfd.get() == -1) {
            BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
                std::system_error, errno, std::system_category,
                "Failed to open temporary directory");
        }

        const auto downloaded_files = casClient->downloadBlobsToDirectory(
            std::vector<Digest>(file_digests.cbegin(), file_digests.cend()),
            temp_dirfd.get());
        checkDownloadBlobsResult(downloaded_files);

        for (const auto &file : actionResult.output_files()) {
            createParentDirectory(dirfd, file.path());
            stageDownloadedFile(dirfd, file.path(), file.digest(),
                                file.is_executable(), temp_dirfd.get(),
                                downloaded_files, duplicate_file_digests);
        }
        for (const auto &symlink : actionResult.output_symlinks()) {
            createParentDirectory(dirfd, symlink.path());
            if (symlinkat(symlink.target().c_str(), dirfd,
                          symlink.path().c_str()) < 0) {
                BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
                    std::system_error, errno, std::system_category,
                    "Failed to create symlink");
            }
        }
        for (const auto &dir : actionResult.output_directories()) {
            const auto dir_digest =
                tree_digest_root_digest_map[dir.tree_digest()];
            createParentDirectory(dirfd, dir.path());
            stageDownloadedDirectory(dirfd, dir.path(), dir_digest,
                                     temp_dirfd.get(), digest_directory_map,
                                     downloaded_files, duplicate_file_digests);
        }
    }
    catch (...) {
        FileUtils::deleteDirectory(dirfd, tempDirectoryName.c_str());
        throw;
    }
    FileUtils::deleteDirectory(dirfd, tempDirectoryName.c_str());
}

} // namespace buildboxcommon
