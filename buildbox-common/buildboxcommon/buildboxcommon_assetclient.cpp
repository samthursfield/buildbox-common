/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcommon_assetclient.h>

#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_grpcerror.h>

#include <buildboxcommon_protos.h>

#include <chrono>
#include <string>

namespace buildboxcommon {

using namespace build::bazel::remote::asset::v1;

void AssetClient::init()
{
    const std::shared_ptr<grpc::Channel> channel = d_grpcClient->channel();

    std::shared_ptr<Fetch::Stub> fetchClient = Fetch::NewStub(channel);
    std::shared_ptr<Push::Stub> pushClient = Push::NewStub(channel);
    init(fetchClient, pushClient);
}

void AssetClient::init(std::shared_ptr<Fetch::StubInterface> fetchClient,
                       std::shared_ptr<Push::StubInterface> pushClient)
{
    this->d_fetchClient = fetchClient;
    this->d_pushClient = pushClient;
}

FetchBlobResponse
AssetClient::fetchBlob(const FetchBlobRequest &request,
                       GrpcClient::RequestStats *requestStats)
{
    FetchBlobResponse response;
    d_grpcClient->issueRequest(
        [&](grpc::ClientContext &context) {
            return d_fetchClient->FetchBlob(&context, request, &response);
        },
        __func__, requestStats);

    return response;
}

AssetClient::FetchResult AssetClient::fetchBlob(
    const std::vector<std::string> &uris,
    const std::vector<std::pair<std::string, std::string>> &qualifiers,
    GrpcClient::RequestStats *requestStats)
{
    FetchBlobRequest remoteRequest;

    for (const std::string &uri : uris) {
        remoteRequest.add_uris(uri);
    }

    for (const std::pair<std::string, std::string> &qualifier : qualifiers) {
        Qualifier *const ptr = remoteRequest.add_qualifiers();
        ptr->set_name(qualifier.first);
        ptr->set_value(qualifier.second);
    }

    remoteRequest.set_instance_name(d_grpcClient->instanceName());

    FetchBlobResponse response = fetchBlob(remoteRequest, requestStats);

    std::vector<std::pair<std::string, std::string>> convertedQualifiers;
    convertedQualifiers.reserve(response.qualifiers().size());
    for (const Qualifier &qualifier : response.qualifiers()) {
        convertedQualifiers.emplace_back(qualifier.name(), qualifier.value());
    }

    return FetchResult{
        response.uri(), std::move(convertedQualifiers),
        std::chrono::time_point<std::chrono::steady_clock>(
            std::chrono::duration_cast<std::chrono::steady_clock::duration>(
                std::chrono::seconds(response.expires_at().seconds()) +
                std::chrono::nanoseconds(response.expires_at().nanos()))),
        response.blob_digest()};
}

FetchDirectoryResponse
AssetClient::fetchDirectory(const FetchDirectoryRequest &request,
                            GrpcClient::RequestStats *requestStats)
{
    FetchDirectoryResponse response;
    d_grpcClient->issueRequest(
        [&](grpc::ClientContext &context) {
            return d_fetchClient->FetchDirectory(&context, request, &response);
        },
        __func__, requestStats);

    return response;
}

AssetClient::FetchResult AssetClient::fetchDirectory(
    const std::vector<std::string> &uris,
    const std::vector<std::pair<std::string, std::string>> &qualifiers,
    GrpcClient::RequestStats *requestStats)
{
    FetchDirectoryRequest remoteRequest;
    for (const std::string &uri : uris) {
        remoteRequest.add_uris(uri);
    }

    for (const std::pair<std::string, std::string> &qualifier : qualifiers) {
        Qualifier *const ptr = remoteRequest.add_qualifiers();
        ptr->set_name(qualifier.first);
        ptr->set_value(qualifier.second);
    }

    remoteRequest.set_instance_name(d_grpcClient->instanceName());

    FetchDirectoryResponse response =
        fetchDirectory(remoteRequest, requestStats);

    std::vector<std::pair<std::string, std::string>> convertedQualifiers;
    convertedQualifiers.reserve(response.qualifiers().size());
    for (const Qualifier &qualifier : response.qualifiers()) {
        convertedQualifiers.emplace_back(qualifier.name(), qualifier.value());
    }

    return FetchResult{
        response.uri(), std::move(convertedQualifiers),
        std::chrono::time_point<std::chrono::steady_clock>(
            std::chrono::duration_cast<std::chrono::steady_clock::duration>(
                std::chrono::seconds(response.expires_at().seconds()) +
                std::chrono::nanoseconds(response.expires_at().nanos()))),
        response.root_directory_digest()};
}

PushBlobResponse AssetClient::pushBlob(const PushBlobRequest &request,
                                       GrpcClient::RequestStats *requestStats)
{
    PushBlobResponse response;
    d_grpcClient->issueRequest(
        [&](grpc::ClientContext &context) {
            return d_pushClient->PushBlob(&context, request, &response);
        },
        __func__, requestStats);

    return response;
}

void AssetClient::pushBlob(
    const std::vector<std::string> &uris,
    const std::vector<std::pair<std::string, std::string>> &qualifiers,
    const Digest &digest, GrpcClient::RequestStats *requestStats)
{
    PushBlobRequest remoteRequest;
    for (const std::string &uri : uris) {
        remoteRequest.add_uris(uri);
    }

    for (const std::pair<std::string, std::string> &qualifier : qualifiers) {
        Qualifier *const ptr = remoteRequest.add_qualifiers();
        ptr->set_name(qualifier.first);
        ptr->set_value(qualifier.second);
    }

    *remoteRequest.mutable_blob_digest() = digest;

    remoteRequest.set_instance_name(d_grpcClient->instanceName());

    pushBlob(remoteRequest, requestStats);
}

PushDirectoryResponse
AssetClient::pushDirectory(const PushDirectoryRequest &request,
                           GrpcClient::RequestStats *requestStats)
{
    PushDirectoryResponse response;
    d_grpcClient->issueRequest(
        [&](grpc::ClientContext &context) {
            return d_pushClient->PushDirectory(&context, request, &response);
        },
        __func__, requestStats);
    return response;
}

void AssetClient::pushDirectory(
    const std::vector<std::string> &uris,
    const std::vector<std::pair<std::string, std::string>> &qualifiers,
    const Digest &rootDirDigest, GrpcClient::RequestStats *requestStats)
{
    PushDirectoryRequest remoteRequest;
    for (const std::string &uri : uris) {
        remoteRequest.add_uris(uri);
    }

    for (const std::pair<std::string, std::string> &qualifier : qualifiers) {
        Qualifier *const ptr = remoteRequest.add_qualifiers();
        ptr->set_name(qualifier.first);
        ptr->set_value(qualifier.second);
    }

    *remoteRequest.mutable_root_directory_digest() = rootDirDigest;

    remoteRequest.set_instance_name(d_grpcClient->instanceName());

    pushDirectory(remoteRequest, requestStats);
}
} // namespace buildboxcommon
