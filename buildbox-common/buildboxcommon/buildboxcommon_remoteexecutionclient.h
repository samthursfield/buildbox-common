// Copyright 2018-2021 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef INCLUDED_BUILDBOXCOMMON_REMOTEEXECUTIONCLIENT
#define INCLUDED_BUILDBOXCOMMON_REMOTEEXECUTIONCLIENT

#include <buildboxcommon_casclient.h>
#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_grpcclient.h>
#include <buildboxcommon_protos.h>

#include <atomic>
#include <map>
#include <set>

namespace buildboxcommon {

typedef std::shared_ptr<grpc::ClientAsyncReaderInterface<Operation>>
    ReaderPointer;

typedef std::shared_ptr<google::longrunning::Operation> OperationPointer;

class RemoteExecutionClient {
  private:
    std::shared_ptr<buildboxcommon::GrpcClient> d_executionGrpcClient;
    std::shared_ptr<buildboxcommon::GrpcClient> d_actionCacheGrpcClient;

    std::shared_ptr<Execution::StubInterface> d_executionStub;
    std::shared_ptr<Operations::StubInterface> d_operationsStub;
    std::shared_ptr<ActionCache::StubInterface> d_actionCacheStub;

    bool readOperation(grpc::CompletionQueue *cq, ReaderPointer &reader,
                       OperationPointer &operation_ptr,
                       const std::atomic_bool &stop_requested,
                       const gpr_timespec &firstRequestDeadline,
                       grpc::Status *errorStatus, bool wait = true);

    /**
     * Sends the CancelOperation RPC
     */
    void cancelOperation(const std::string &operationName);

    OperationPointer submitExecution(const Digest &actionDigest,
                                     const std::atomic_bool &stop_requested,
                                     bool skipCache,
                                     const ExecutionPolicy *executionPolicy,
                                     bool wait);

  public:
    explicit RemoteExecutionClient(
        std::shared_ptr<buildboxcommon::GrpcClient> executionGrpcClient,
        std::shared_ptr<buildboxcommon::GrpcClient> actionCacheGrpcClient)
        : d_executionGrpcClient(executionGrpcClient),
          d_actionCacheGrpcClient(actionCacheGrpcClient)
    {
    }

    void init(std::shared_ptr<Execution::StubInterface> executionStub,
              std::shared_ptr<ActionCache::StubInterface> actionCacheStub,
              std::shared_ptr<Operations::StubInterface> operationsStub);

    void init();

    /**
     * Attempts to fetch the ActionResult with the given digest from the action
     * cache and store it in the `result` parameter. The return value
     * indicates whether the ActionResult was found in the action cache.
     * If it wasn't, `result` is not modified.
     *
     */
    bool fetchFromActionCache(const Digest &actionDigest,
                              const std::set<std::string> &outputs,
                              ActionResult *result);

    /**
     * Upload a new execution result. The Action message must be uploaded to
     * CAS before calling this method.
     */
    void updateActionCache(const Digest &actionDigest,
                           const ActionResult &result);

    /**
     * Run the action with the given digest on the given server, waiting
     * synchronously for it to complete. The Action must already be present in
     * the server's CAS.
     */
    ActionResult
    executeAction(const Digest &actionDigest,
                  const std::atomic_bool &stop_requested,
                  bool skipCache = false,
                  const ExecutionPolicy *executionPolicy = nullptr);

    /**
     * Run the action with the given digest on the given server asynchronously.
     * Returns the operation id. The Action must already be present in
     * the server's CAS.
     */
    google::longrunning::Operation
    asyncExecuteAction(const Digest &actionDigest,
                       const std::atomic_bool &stop_requested,
                       bool skipCache = false,
                       const ExecutionPolicy *executionPolicy = nullptr);

    /**
     * Download all output directories and files and store them in the
     * specified directory.
     *
     * Will throw an exception if one or multiple blobs cannot be downloaded.
     */
    void downloadOutputs(buildboxcommon::CASClient *casClient,
                         const ActionResult &actionResult, int dirfd);

    /*
     * Gets the current Operation object by operation id. Used to check its
     * execution status.
     */
    google::longrunning::Operation
    getOperation(const std::string &operationName);
};
} // namespace buildboxcommon
#endif
